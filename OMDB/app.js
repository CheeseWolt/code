var submit = document.getElementById('submit');
var film = document.getElementById('film');
var display = document.getElementById('display')
var resume = document.getElementById('resume');
var xhttp = new XMLHttpRequest();


submit.addEventListener('click', fetchFilm);
var json;
function fetchFilm() {
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            // on retire les elements dans la div #display et dans la div #resume
            while (display.firstChild) {
                display.removeChild(display.firstChild);
            }
            while(resume.firstChild){
                resume.removeChild(resume.firstChild);
            }
            result = JSON.parse(this.response).Search;
            for (let a in result) {
                let card = document.createElement('div');       // creation de la div pour le film
                card.addEventListener('click',test);
                card.id=result[a].imdbID;
                display.appendChild(card);                      //liaison de la div #card à la div #display
                card.classList.add("card");                     //ajout de la classe .card à la div #card

                let titre = document.createElement('li');       //creation du span pour le titre du film
                card.appendChild(titre);                        //liaison du span à #card
                titre.innerHTML = result[a].Title + ' ('+result[a].Year+')'; //on met le titre contenu dans le json entre les balises

                let poster = document.createElement('img');     //creation d'une balise img
                card.appendChild(poster);                       //liaison de la balise à la #card
                poster.src = result[a].Poster;                  //ajout de la source de l'image à partir du json
            }
        }
    };
    xhttp.open("GET", "http://www.omdbapi.com/?apikey=c537c65c&s=" + film.value, true);
    xhttp.send();
}

function test(){
    while (resume.firstChild) {
        resume.removeChild(resume.firstChild);
    }
    xhttp.onreadystatechange = function(){
        if(this.readyState==4 && this.status == 200){
            result = JSON.parse(this.responseText);
            let titre = document.createElement('h1');
            titre.innerHTML = result.Title + ' ('+ result.Year+')';
            resume.appendChild(titre);

            let soustitre = document.createElement('h2');
            soustitre.innerHTML='Sortie : '+result.Released;
            resume.appendChild(soustitre);

            let poster = document.createElement('img');
            poster.src = result.Poster;
            resume.appendChild(poster);

            let plot = document.createElement('p');
            plot.innerHTML = result.Plot;
            resume.appendChild(plot);

        }
    };
    xhttp.open("GET", "http://www.omdbapi.com/?apikey=c537c65c&i="+ this.id +"&plot=full", true);
    xhttp.send();
}

var adresse = {
    numero: "1",
    rue: "Hegel",
    ville: "Lille",
    cp: 59000,
    afficher : function(){
        console.log("Mon adresse est : " + this.numero + " rue "+ this.rue + " à "+ this.ville + " " + this.cp);
    }
}


var personne= {
    nom : "Routier",
    prenom : "Benoît",
    tel : "0673420528",
    email : "benoit.routier7@gmail.com",
    afficher : function() {
        console.log('Mon nom est ' +this.nom+ ' '+this.prenom+' vous pouvez me contacter au '+this.tel+ ' ou par mail '+this.email);
    }
}

console.log(personne);
personne.afficher();
const { clavier } = require('./clavier')


/**
 * Retourne nb1 + nb2
 * @param {number} nb1 
 * @param {number} nb2 
 * @returns {number} le resultat de l'addition
 */
const add = (nb1, nb2) => {
    return parseFloat(nb1) + parseFloat(nb2);
}


const diff = (nb1, nb2) => {
    return nb1 - nb2;
}

const mult = (nb1, nb2) => {
    return nb1 * nb2;
}

const div = (nb1, nb2) => {
    return nb1 / nb2;
}
const carre = (nb1, ) => {
    return nb1 * nb1;
}
const racine = (nb1, ) => {
    return Math.sqrt(nb1);
}
/**
 * Retourne le calcul saisit dans input
 * @param {String} input la chaine de caractère contenant le calcul
 * @returns {number} Le résultat du calcul
 */
const calculette = (input) => {
    console.log("calcul : ", input)
    input = input.trim();
    let RegNb = new RegExp(/\D/); // Expression régulière pour extraire les nombres
    let RegOp = new RegExp(/\d/); // Expression régulière pour extraire les NON-nombres
    let tabOp = input.split(RegOp);
    let nb = input.split(RegNb);
    // console.log(tabOp);
    // console.log(nb);
    for (let char of tabOp) {
        if (isNaN(char)) {
            op = char;
        }
    }

    switch (op) {
        case '+':
            console.log(add(nb[0], nb[1]));
            break;
        case '-':
            console.log(diff(nb[0], nb[1]));
            break;
        case '*':
            console.log(mult(nb[0], nb[1]));
            break;
        case '/':
            console.log(div(nb[0], nb[1]));
            break;
        case '²':
            console.log(carre(nb[0]));
            break;
        case 'v':
            console.log(racine(nb[1]));
            break;
        default:
            return "Opération inconnue, veuillez saisir + - * ou /"
            break;
    }
}

clavier(calculette);
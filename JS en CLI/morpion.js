const { clavier } = require('./clavier');

// // TP MORPION
// 1) Ecrire une fonction nouveau() qui retourne un jeu vide, c'est à dire une liste de 8 cases contenant '.' dans chaque case.
const nouveau = () => {
    let arr = Array(9)
    for (let i = 0; i < arr.length; i++) {
        arr[i] = '.';

    }
    return arr;
}
// 2) Ecrire une fonction affiche(jeu) capable d'afficher un jeu passé en paramètre.
const affiche = (jeu) => {
    let ligne = '';
    for (let i = 0; i < 9; i++) {
        ligne += jeu[i];
        ligne += ' ';
        if ((i + 1) % 3 === 0 && i !== 0) {
            console.log(ligne);
            ligne = '';
        }
    }
}
// 3) Ecrire une fonction joueur(jeu) qui permet de savoir qui doit jouer à partir d'un jeu passé en paramètre
const joueur = (jeu) => {
    let x = 0;
    let o = 0;
    for (let i = 0; i < jeu.length; i++) {
        jeu[i] === 'x' ? x++ : null;
        jeu[i] === 'o' ? o++ : null;
    }
    return x >= o === true ? 'o' : 'x';
}
// 4) Ecrire une fonction coupPossibles(jeu) qui retourne les coups possibles à partir d'un jeu passé en paramètre.
const coupPossibles = (jeu) => {
    let coup = [];
    let idx = jeu.indexOf('.');
    while (idx != -1) {
        coup.push(idx);
        idx = jeu.indexOf('.', idx + 1);
    }
    return coup;
}
// 5)fonction gagner(jeu) qui permet de savoir qui a gagné ou perdu. Cette fonction retourne :
// - 1 si le joueur 'X' a gagné.
// - (-1) si le joueur 'O' a gagné.
// 0 si aucun joueur 2 a gagné ( qu'il reste des coups possibles ou non)
const gagner = (jeu) => {
    let winner = 0;
    //check ligne
    for (let col = 0; col <= 6; col += 3) {
        if (jeu[col] === jeu[col + 1] && jeu[col + 1] === jeu[col + 2]) {
            winner = jeu[col] != '.' ? jeu[col] == 'x' ? 1 : -1 : 0;
        }
    }
    //check colonne
    for (let row = 0; row < jeu.length / 3; row++) {
        if (jeu[row] === jeu[row + 3] && jeu[row + 3] === jeu[row + 6]) {
            winner = jeu[row] != '.' ? jeu[row] == 'x' ? 1 : -1 : 0;
        }
    }
    if (((jeu[0] === jeu[4] && jeu[4] === jeu[8]) || (jeu[2] === jeu[4] && jeu[4] === jeu[6])) && jeu[4] != '.') {
        winner = jeu[4] != '.' ? jeu[4] == 'x' ? 1 : -1 : 0;
    }
    return winner;
}
// 6) Ecrire une fonction jouer(jeu, coup) qui permet de jouer un coup et qui retourne le nouveau jeu. Cette fonction prend en paramètre un jeu et un numéro de la case.
// Elle appelle la fonction joueur pour savoir quel est le joueur qui a joué et elle ajoute le bon caractère au bon endroit dans le jeux.

const tourIa = (jeu) => {
    console.log('Tour de l\'IA :');
    
    let cp = coupPossibles(jeu);
        jeu[ Math.floor(Math.random() * cp.length)] = joueur(jeu);
        console.log("-----");
        affiche(jeu);
        return jeu;
}


/**
 * Jouer la case saisie par l'utilisateur
 * @param {String} valeur
 */
let jeu = nouveau();

function tourJoueur(valeur) {
    affiche(jeu)
    let coup = valeur.trim();
    let cp = coupPossibles(jeu);
    let test = false;
    //console.log(coup.split(""));
    //console.log(cp.indexOf(coup));
    //if (cp.indexOf(coup) != -1) {
    if (joueur(jeu) == 'o') {
        for (var i = 0; i < cp.length; i++) {
            if (cp[i] == coup) {
                test = true;
            }
        }
        //console.log("Jouer quelle case?");
        if (test) {
            jeu[coup] = joueur(jeu);
            affiche(jeu);
        }
        clavier(tourJoueur);
    } else {
        tourIa(jeu);
    }

    console.log('------');
    return jeu;
}


// while (coupPossibles(jeu).length > 0) {
//     console.log("Case(s) disponnibles :", coupPossibles(jeu));
//     let coup = coupPossibles(jeu);
//     winner = gagner(jeu);
//     if (winner !== 0) {
//         break;
//     }
// }


// affiche(jeu);
// let winner = 0;
clavier(tourJoueur);
// affiche(jeu);
// console.log("=======");
// switch (winner) {
//     case 1:
//         console.log('X gagne');
//         break;
//     case -1:
//         console.log('O gagne');
//         break;
//     case 0:
//         console.log('Match nul');
//         break;
// }
// console.log("=======");


const terminal = process.stdin;


/**
 * fonction de rappel qui recevra une touche clavier
 * @callback actionTouche
 * @param {String} text  Chaîne de caractère
 */

/**
 * Permet de récupérer les touches du clavier
 * @param {actionTouche} callback - fonction de rappel qui recevra une touche clavier
 * @return void
 */
exports.clavier = function(callback) {

    terminal.on("data", (touche) => {
        const valeur = touche.toString();

        callback(valeur);
    })
}
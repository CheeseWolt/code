const add = (nb1, nb2) => {
    return parseFloat(nb1) + parseFloat(nb2);
}

const diff = (nb1, nb2) => {
    return nb1-nb2;
}

const mult = (nb1,nb2) => {
    return nb1*nb2;
}

const div = (nb1, nb2) => {
    return nb1/nb2 ;
}

const calculette = (input) =>{
    console.log("calcul : ", input)
    let RegNb = new RegExp(/\D/); // Expression régulière pour extraire les nombres
    let RegOp = new RegExp(/\d/); // Expression régulière pour extraire les NON-nombres
    let tabOp = input.split(RegOp);
    let nb = input.split(RegNb);

    for(let char of tabOp){
        if(isNaN(char)){
            op = char;
        }
    }

    switch(op)
    {
        case '+':
            return add(nb[0], nb[1]);
        break;        
        case '-':
            return diff(nb[0],nb[1]);
        break;
        case '*':
            return mult(nb[0],nb[1]);
        break;
        case '/':
            return div(nb[0],nb[1]);
        break;
        default:
            return "Opération inconnue, veuillez saisir + - * ou /"
        break;
    }
}

console.log(calculette('60+4'));
console.log(calculette('4-7'));
console.log(calculette('8*4'));
console.log(calculette('5/8'));

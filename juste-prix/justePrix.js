// Le Juste Prix
// deviner le prix d'un article si le prix est faux il faut dire si le prix est en dessous ou au dessu
const TMAX = 6;
const input     = document.getElementById('prix');
const output   = document.getElementById('output');
const counter     = document.getElementById('tentative')
const button = document.getElementById('button');

var prix = 0;
var tentative = 0;
const deviner = () => {
    if (tentative < TMAX) {
        tentative++;
        console.log("Essai numéro " + tentative);
        counter.innerHTML = '<p>'+tentative+'</p>'
        if (input.value == prix) {
            console.log("Trouvé")
            output.innerHTML = '<p>Vous avez trouver le juste prix</p>'
            nouveau();
        } else {
            guess = Math.sign(input.value - prix);
            if (guess > 0) {
                output.innerHTML = '<p>C\'est moins</p>'
                console.log("C'est moins")
            } else {
                output.innerHTML = '<p>C\'est plus</p>'
                console.log("C'est plus")
            }
        }
    }
    else {
        output.innerHTML = '<p>Nombre de tenative dépassée</p>'
        console.log("Nombre de tenative dépassée");
        nouveau();
    }
}

const nouveau = () => {
    tentative = 0;
    prix = Math.floor(Math.random() * 20);
}
button.addEventListener('click', deviner);
window.addEventListener('load',nouveau);
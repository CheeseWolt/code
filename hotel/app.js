const Client = require('./Client');
const Hotel = require('./Hotel');
const Chambre = require('./Chambre');
const {clavier} = require('./clavier');
const hotel = new Hotel('California', 8);
hotel.reserverChambre(new Client('Jean', 'Dupond', '23/07/2019', '28/07/2019'));
hotel.reserverChambre(new Client('Eddy','Galmand','23/07/2019','28/07/2019'));
hotel.reserverChambre(new Client('Circé','Perlain','23/07/2019','28/07/2019'));
hotel.reserverChambre(new Client('Coline','Tylec','23/07/2019','28/07/2019'));
hotel.reserverChambre(new Client('Julien','Vankemel','23/07/2019','28/07/2019'));
hotel.reserverChambre(new Client('Quentin','Debtil','23/07/2019','28/07/2019'));

const Menu = (str) =>{
    switch (str.trim()) {
        case '1':
            hotel.afficherNbChambreOQP();
            break;
        case '2':
            hotel.afficherChambreVide();
            break;
        case '3':
            hotel.afficherClient();
            break;
        case '4':
            hotel.afficherChambreOQP();
            break;

        default:
            break;
    }
}
clavier(Menu);
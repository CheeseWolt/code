class Chambre{
    constructor(numero,client,dateReservationDebut,dateReservationFin){
        this.numero = numero;
        this.client = client;
        this.dateReservationDebut = dateReservationDebut;
        this.dateReservationFin = dateReservationFin;
    }
    afficheChambre(){
        console.log('numero : '+this.numero+', client : '+(this.client?this.client:'personne ')+ (this.dateReservationDebut?', reservée du '+this.dateReservationDebut+' au '+this.dateReservationFin:''));
    }
    toString(){
        console.log(this);
    }
}
// const chambre = new Chambre(123,'Marcel','23/01/1989','07/06/1993');
// const chambreVide = new Chambre(123);

// chambre.afficheChambre();
// chambreVide.afficheChambre();
module.exports = Chambre;
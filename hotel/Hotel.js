const Chambre = require('./Chambre');

class Hotel {
    constructor(nomHotel, nbChambreTotal) {
        this.nomHotel = nomHotel;
        this.nbChambreTotal = nbChambreTotal;
        this.tabClient = [];
        this.tabChambre = [];
        this.tabHistorique = [];
        this.nbChambreOQP = 0;
        for (let i=0 ; i< this.nbChambreTotal ; i++){
            this.tabChambre[i] = new Chambre();
        }
    }

    afficherNbChambreOQP = function () {
        console.log(this.tabClient.length);
    }
    afficherChambreVide = function () {
        console.log(this.nbChambreTotal - this.tabClient.length);
    }
    afficherClient = function () {
        console.log(this.tabClient);
    }
    afficherTabChambre = function(){
        console.log(this.tabChambre);
    }
    reserverChambre = function (client) {
        this.tabClient.push(client);
        this.tabChambre[this.nbChambreOQP] = new Chambre(this.nbChambreOQP,client.nom,client.dateArrivee, client.dateFin);
        this.nbChambreOQP++;
    }
    afficherChambreOQP = function(){
        console.log(this.tabChambre);

        for(let i = 0 ; i< this.nbChambreOQP ; i++){
            console.log(this.tabChambre[i]);
        }
    }
}
module.exports = Hotel;
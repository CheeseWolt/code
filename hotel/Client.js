// const { clavier } = require('./clavier');

class Client {
    constructor(nom, prenom, dateArrivee, dateFin){
        let dateA = dateArrivee.split('/');
        let dateF = dateFin.split('/');

        this.nom = nom;
        this.prenom = prenom;
        let D1 = new Date(dateA[2],dateA[1],dateA[0]);
        let D2 = new Date(dateF[2],dateF[1],dateF[0]);
        this.dateArrivee = D1.toDateString();
        this.dateFin = D2.toDateString();
    }
    afficheSejour(){
        console.log("Le client a reservé du "+this.dateArrivee+" au "+this.dateFin);
    }
    calcul(){
        let duree = new Date(this.dateFin-this.dateArrivee).getDate();
        console.log(27*duree);
    }
}
// const client = new Client('Jean','Dupont','24/07/2019','27/07/2019');
// console.log(client.afficheSejour());
// console.log(client);
module.exports = Client;
